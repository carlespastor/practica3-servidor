<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Unirest;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        // search Songs of Frank Sinatra
        $headers = array('Accept' => 'application/json');
        //$query = array('q' => 'Frank sinatra', 'type' => 'track');

        $response = Unirest\Request::get('http://localhost:8000/quest');
        $quests = ($response->body);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'quests' => $quests,
        ));
    }
}
