<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quest
 *
 * @ORM\Table(name="quest")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestRepository")
 */
class Quest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="quest", type="string", length=255)
     */
    private $quest;

    /**
     * @var string
     *
     * @ORM\Column(name="ans1", type="string", length=255)
     */
    private $ans1;

    /**
     * @var string
     *
     * @ORM\Column(name="ans2", type="string", length=255)
     */
    private $ans2;

    /**
     * @var string
     *
     * @ORM\Column(name="ans3", type="string", length=255)
     */
    private $ans3;

    /**
     * @var string
     *
     * @ORM\Column(name="ans4", type="string", length=255)
     */
    private $ans4;

    /**
     * @var int
     *
     * @ORM\Column(name="ans", type="integer")
     */
    private $ans;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quest
     *
     * @param string $quest
     *
     * @return Quest
     */
    public function setQuest($quest)
    {
        $this->quest = $quest;

        return $this;
    }

    /**
     * Get quest
     *
     * @return string
     */
    public function getQuest()
    {
        return $this->quest;
    }

    /**
     * Set ans1
     *
     * @param string $ans1
     *
     * @return Quest
     */
    public function setAns1($ans1)
    {
        $this->ans1 = $ans1;

        return $this;
    }

    /**
     * Get ans1
     *
     * @return string
     */
    public function getAns1()
    {
        return $this->ans1;
    }

    /**
     * Set ans2
     *
     * @param string $ans2
     *
     * @return Quest
     */
    public function setAns2($ans2)
    {
        $this->ans2 = $ans2;

        return $this;
    }

    /**
     * Get ans2
     *
     * @return string
     */
    public function getAns2()
    {
        return $this->ans2;
    }

    /**
     * Set ans3
     *
     * @param string $ans3
     *
     * @return Quest
     */
    public function setAns3($ans3)
    {
        $this->ans3 = $ans3;

        return $this;
    }

    /**
     * Get ans3
     *
     * @return string
     */
    public function getAns3()
    {
        return $this->ans3;
    }

    /**
     * Set ans4
     *
     * @param string $ans4
     *
     * @return Quest
     */
    public function setAns4($ans4)
    {
        $this->ans4 = $ans4;

        return $this;
    }

    /**
     * Get ans4
     *
     * @return string
     */
    public function getAns4()
    {
        return $this->ans4;
    }

    /**
     * Set ans
     *
     * @param integer $ans
     *
     * @return Quest
     */
    public function setAns($ans)
    {
        $this->ans = $ans;

        return $this;
    }

    /**
     * Get ans
     *
     * @return int
     */
    public function getAns()
    {
        return $this->ans;
    }
}

