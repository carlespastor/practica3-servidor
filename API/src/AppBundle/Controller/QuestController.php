<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Quest;

class QuestController extends FOSRestController
{
	/**
     * @Rest\Get("/quest")
     */
    public function getAction()
    {
      $restresult = $this->getDoctrine()->getRepository('AppBundle:Quest')->findAll();
        if ($restresult === null) {
          return new View("there are no users exist", Response::HTTP_NOT_FOUND);
     }
        return $restresult;
    }

    /**
	* @Rest\Get("/quest/{id}")
	*/
	public function idAction($id)
	{
		$singleresult = $this->getDoctrine()->getRepository('AppBundle:Quest')->find($id);
		if ($singleresult === null) {
		  	return new View("Quest not found", Response::HTTP_NOT_FOUND);
		}
		return $singleresult;
	}


	/**
	* @Rest\Post("/quest/")
	*/
	public function postAction(Request $request)
	{
		$data = new Quest;
		$id = $request->get('id');
		$quest = $request->get('quest');
		$ans1 = $request->get('ans1');
		$ans2 = $request->get('ans2');
		$ans3 = $request->get('ans3');
		$ans4 = $request->get('ans4');
		$ans = $request->get('ans');
		if(empty($id) || empty($quest) || empty($ans1) || empty($ans2) || empty($ans3) || empty($ans4) || empty($ans)){
			return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE); 
		} 
		$data->setId($id);
		$data->setQuest($quest);
		$data->setAns1($ans1);
		$data->setAns2($ans2);
		$data->setAns3($ans3);
		$data->setAns4($ans4);
		$data->setAns($ans);
		$em = $this->getDoctrine()->getManager();
		$em->persist($data);
		$em->flush();

		return new View("Quest Added Successfully", Response::HTTP_OK);
	}

	  /**
	 * @Rest\Put("/quest/{id}")
	 */
	 public function updateAction($id,Request $request)
	 { 
	 $data = new Quest;
	 $quest = $request->get('quest');
	 $ans1 = $request->get('ans1');
	 $ans2 = $request->get('ans2');
	 $ans3 = $request->get('ans3');
	 $ans4 = $request->get('ans4');
	 $ans = $request->get('ans');
	 $quest = $this->getDoctrine()->getRepository('AppBundle:Quest')->find($id);
	if (empty($quest)) {
	   return new View("Quest not found", Response::HTTP_NOT_FOUND);
	 } 
	elseif(!empty($quest) && !empty($ans1) && !empty($ans2) && !empty($ans3) && !empty($ans4) && !empty($ans)){
	   $quest->setQuest($quest);
		$quest->setAns1($ans1);
		$quest->setAns2($ans2);
		$quest->setAns3($ans3);
		$quest->setAns4($ans4);
		$quest->setAns($ans);
	   $sn->flush();
	   return new View("Quest Updated Successfully", Response::HTTP_OK);
	 }
	else return new View("There are empty fields", Response::HTTP_NOT_ACCEPTABLE); 
	 }

	/**
	 * @Rest\Delete("/quest/{id}")
	 */
	 public function deleteAction($id)
	 {
	  $data = new Quest;
	  $sn = $this->getDoctrine()->getManager();
	  $quest = $this->getDoctrine()->getRepository('AppBundle:Quest')->find($id);
	if (empty($quest)) {
	  return new View("Quest not found", Response::HTTP_NOT_FOUND);
	 }
	 else {
	  $sn->remove($quest);
	  $sn->flush();
	 }
	  return new View("deleted successfully", Response::HTTP_OK);
	 }
}